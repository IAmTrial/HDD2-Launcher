﻿namespace HDD2_Launcher.Source {
    partial class OptionsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsForm));
            this.OptionsTabControl = new System.Windows.Forms.TabControl();
            this.GameExeTabPage = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.GameExeLocationLabel = new System.Windows.Forms.Label();
            this.GameExeLocationTextBox = new System.Windows.Forms.TextBox();
            this.GameExeChooseButton = new System.Windows.Forms.Button();
            this.MapHackConfigsLabel = new System.Windows.Forms.Label();
            this.MapHackConfigsLocationTextBox = new System.Windows.Forms.TextBox();
            this.MapHackConfigsLocationChooseButton = new System.Windows.Forms.Button();
            this.GameExeOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.MapHackConfigsLocationFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.GameExeFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.EnableBHCheckBox = new System.Windows.Forms.CheckBox();
            this.MiscTabPage = new System.Windows.Forms.TabPage();
            this.OptionsTabControl.SuspendLayout();
            this.GameExeTabPage.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.MiscTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // OptionsTabControl
            // 
            this.OptionsTabControl.Controls.Add(this.GameExeTabPage);
            this.OptionsTabControl.Controls.Add(this.MiscTabPage);
            this.OptionsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsTabControl.Location = new System.Drawing.Point(0, 0);
            this.OptionsTabControl.Name = "OptionsTabControl";
            this.OptionsTabControl.SelectedIndex = 0;
            this.OptionsTabControl.Size = new System.Drawing.Size(292, 273);
            this.OptionsTabControl.TabIndex = 0;
            // 
            // GameExeTabPage
            // 
            this.GameExeTabPage.Controls.Add(this.flowLayoutPanel1);
            this.GameExeTabPage.Location = new System.Drawing.Point(4, 22);
            this.GameExeTabPage.Name = "GameExeTabPage";
            this.GameExeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.GameExeTabPage.Size = new System.Drawing.Size(284, 247);
            this.GameExeTabPage.TabIndex = 0;
            this.GameExeTabPage.Text = "Directories";
            this.GameExeTabPage.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.GameExeLocationLabel);
            this.flowLayoutPanel1.Controls.Add(this.GameExeLocationTextBox);
            this.flowLayoutPanel1.Controls.Add(this.GameExeChooseButton);
            this.flowLayoutPanel1.Controls.Add(this.MapHackConfigsLabel);
            this.flowLayoutPanel1.Controls.Add(this.MapHackConfigsLocationTextBox);
            this.flowLayoutPanel1.Controls.Add(this.MapHackConfigsLocationChooseButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(278, 241);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // GameExeLocationLabel
            // 
            this.GameExeLocationLabel.AutoSize = true;
            this.GameExeLocationLabel.Location = new System.Drawing.Point(3, 0);
            this.GameExeLocationLabel.Name = "GameExeLocationLabel";
            this.GameExeLocationLabel.Size = new System.Drawing.Size(144, 13);
            this.GameExeLocationLabel.TabIndex = 0;
            this.GameExeLocationLabel.Text = "Diablo II Game.exe Location:";
            // 
            // GameExeLocationTextBox
            // 
            this.GameExeLocationTextBox.Location = new System.Drawing.Point(3, 16);
            this.GameExeLocationTextBox.Name = "GameExeLocationTextBox";
            this.GameExeLocationTextBox.Size = new System.Drawing.Size(264, 20);
            this.GameExeLocationTextBox.TabIndex = 1;
            this.GameExeLocationTextBox.TextChanged += new System.EventHandler(this.GameExeLocationTextBox_TextChanged);
            // 
            // GameExeChooseButton
            // 
            this.GameExeChooseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GameExeChooseButton.Location = new System.Drawing.Point(192, 42);
            this.GameExeChooseButton.Name = "GameExeChooseButton";
            this.GameExeChooseButton.Size = new System.Drawing.Size(75, 23);
            this.GameExeChooseButton.TabIndex = 1;
            this.GameExeChooseButton.Text = "Select...";
            this.GameExeChooseButton.UseVisualStyleBackColor = true;
            this.GameExeChooseButton.Click += new System.EventHandler(this.GameExeChooseButton_Click);
            // 
            // MapHackConfigsLabel
            // 
            this.MapHackConfigsLabel.AutoSize = true;
            this.MapHackConfigsLabel.Location = new System.Drawing.Point(3, 68);
            this.MapHackConfigsLabel.Name = "MapHackConfigsLabel";
            this.MapHackConfigsLabel.Size = new System.Drawing.Size(158, 13);
            this.MapHackConfigsLabel.TabIndex = 0;
            this.MapHackConfigsLabel.Text = "MapHack Config Files Location:";
            // 
            // MapHackConfigsLocationTextBox
            // 
            this.MapHackConfigsLocationTextBox.Location = new System.Drawing.Point(3, 84);
            this.MapHackConfigsLocationTextBox.Name = "MapHackConfigsLocationTextBox";
            this.MapHackConfigsLocationTextBox.Size = new System.Drawing.Size(264, 20);
            this.MapHackConfigsLocationTextBox.TabIndex = 1;
            this.MapHackConfigsLocationTextBox.TextChanged += new System.EventHandler(this.MapHackConfigsLocationTextBox_TextChanged);
            // 
            // MapHackConfigsLocationChooseButton
            // 
            this.MapHackConfigsLocationChooseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MapHackConfigsLocationChooseButton.Location = new System.Drawing.Point(192, 110);
            this.MapHackConfigsLocationChooseButton.Name = "MapHackConfigsLocationChooseButton";
            this.MapHackConfigsLocationChooseButton.Size = new System.Drawing.Size(75, 23);
            this.MapHackConfigsLocationChooseButton.TabIndex = 1;
            this.MapHackConfigsLocationChooseButton.Text = "Select...";
            this.MapHackConfigsLocationChooseButton.UseVisualStyleBackColor = true;
            this.MapHackConfigsLocationChooseButton.Click += new System.EventHandler(this.MapHackConfigsLocationChooseButton_Click);
            // 
            // GameExeOpenFileDialog
            // 
            this.GameExeOpenFileDialog.Filter = "Game.exe|game.exe";
            this.GameExeOpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.GameExeOpenFileDialog_FileOk);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.EnableBHCheckBox);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(278, 241);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // EnableBHCheckBox
            // 
            this.EnableBHCheckBox.AutoSize = true;
            this.EnableBHCheckBox.Location = new System.Drawing.Point(3, 3);
            this.EnableBHCheckBox.Name = "EnableBHCheckBox";
            this.EnableBHCheckBox.Size = new System.Drawing.Size(93, 17);
            this.EnableBHCheckBox.TabIndex = 0;
            this.EnableBHCheckBox.Text = "Enable Hacks";
            this.EnableBHCheckBox.UseVisualStyleBackColor = true;
            this.EnableBHCheckBox.CheckedChanged += new System.EventHandler(this.EnableBHCheckBox_CheckedChanged);
            // 
            // MiscTabPage
            // 
            this.MiscTabPage.Controls.Add(this.flowLayoutPanel3);
            this.MiscTabPage.Location = new System.Drawing.Point(4, 22);
            this.MiscTabPage.Name = "MiscTabPage";
            this.MiscTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.MiscTabPage.Size = new System.Drawing.Size(284, 247);
            this.MiscTabPage.TabIndex = 1;
            this.MiscTabPage.Text = "Misc";
            this.MiscTabPage.UseVisualStyleBackColor = true;
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.OptionsTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "OptionsForm";
            this.Text = "Options";
            this.OptionsTabControl.ResumeLayout(false);
            this.GameExeTabPage.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.MiscTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl OptionsTabControl;
        private System.Windows.Forms.TabPage GameExeTabPage;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label GameExeLocationLabel;
        private System.Windows.Forms.TextBox GameExeLocationTextBox;
        private System.Windows.Forms.Button GameExeChooseButton;
        private System.Windows.Forms.OpenFileDialog GameExeOpenFileDialog;
        private System.Windows.Forms.Label MapHackConfigsLabel;
        private System.Windows.Forms.TextBox MapHackConfigsLocationTextBox;
        private System.Windows.Forms.Button MapHackConfigsLocationChooseButton;
        private System.Windows.Forms.FolderBrowserDialog MapHackConfigsLocationFolderBrowserDialog;
        private System.Windows.Forms.FolderBrowserDialog GameExeFolderBrowserDialog;
        private System.Windows.Forms.TabPage MiscTabPage;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.CheckBox EnableBHCheckBox;
    }
}