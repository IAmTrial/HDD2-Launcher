﻿namespace HDD2_Launcher.Source {
    #region Using Imports
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using HDD2_Launcher.Properties;
    using System.IO;
    #endregion Using Imports
    public partial class OptionsForm : Form {
        public OptionsForm() {
            InitializeComponent();
            this.GameExeLocationTextBox.Text = Settings.Default.GameInstallLoc;
            this.MapHackConfigsLocationTextBox.Text = Path.GetFullPath(Settings.Default.MHConfigsLoc);
            this.EnableBHCheckBox.Checked = Settings.Default.EnableHacks;
        }

        private void GameExeChooseButton_Click(object sender, EventArgs e) {
            GameExeOpenFileDialog.ShowDialog();
        }

        private void GameExeOpenFileDialog_FileOk(object sender, CancelEventArgs e) {
            GameExeLocationTextBox.Text = Path.GetDirectoryName(GameExeOpenFileDialog.FileName);
        }

        private void GameExeLocationTextBox_TextChanged(object sender, EventArgs e) {
            Settings.Default.GameInstallLoc = GameExeLocationTextBox.Text;
        }

        private void MapHackConfigsLocationChooseButton_Click(object sender, EventArgs e) {
            MapHackConfigsLocationFolderBrowserDialog.ShowDialog(this);
        }

        private void MapHackConfigsLocationTextBox_TextChanged(object sender, EventArgs e) {
            Settings.Default.MHConfigsLoc = MapHackConfigsLocationTextBox.Text;
        }

        private void EnableBHCheckBox_CheckedChanged(object sender, EventArgs e) {
            Settings.Default.EnableHacks = EnableBHCheckBox.Checked;
        }
    }
}
