﻿namespace HDD2_Launcher.Source
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenuItem = new System.Windows.Forms.MenuItem();
            this.OptionsMenuItem = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.HelpMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenuItem = new System.Windows.Forms.MenuItem();
            this.GameVersionLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Version113cRadioButton = new System.Windows.Forms.RadioButton();
            this.Version113dRadioButton = new System.Windows.Forms.RadioButton();
            this.StartGameButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.FullscreenCheckBox = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.NumberOfWindowsComboBox = new System.Windows.Forms.ComboBox();
            this.WindowLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.MapHackComboBox = new System.Windows.Forms.ComboBox();
            this.MaphackConfigLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.AdditionalParamsLabel = new System.Windows.Forms.Label();
            this.AdditionalParamsTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenuItem,
            this.HelpMenuItem});
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.Index = 0;
            this.FileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.OptionsMenuItem,
            this.ExitMenuItem});
            this.FileMenuItem.Text = "File";
            // 
            // OptionsMenuItem
            // 
            this.OptionsMenuItem.Index = 0;
            this.OptionsMenuItem.Text = "Options";
            this.OptionsMenuItem.Click += new System.EventHandler(this.OptionsMenuItem_Click);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 1;
            this.ExitMenuItem.Text = "Exit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // HelpMenuItem
            // 
            this.HelpMenuItem.Index = 1;
            this.HelpMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.AboutMenuItem});
            this.HelpMenuItem.Text = "Help";
            // 
            // AboutMenuItem
            // 
            this.AboutMenuItem.Index = 0;
            this.AboutMenuItem.Text = "About";
            // 
            // GameVersionLabel
            // 
            this.GameVersionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GameVersionLabel.AutoSize = true;
            this.GameVersionLabel.Location = new System.Drawing.Point(3, 19);
            this.GameVersionLabel.Name = "GameVersionLabel";
            this.GameVersionLabel.Size = new System.Drawing.Size(45, 13);
            this.GameVersionLabel.TabIndex = 1;
            this.GameVersionLabel.Text = "Version:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.Version113cRadioButton);
            this.flowLayoutPanel1.Controls.Add(this.Version113dRadioButton);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(54, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(58, 46);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // Version113cRadioButton
            // 
            this.Version113cRadioButton.AutoSize = true;
            this.Version113cRadioButton.Location = new System.Drawing.Point(3, 3);
            this.Version113cRadioButton.Name = "Version113cRadioButton";
            this.Version113cRadioButton.Size = new System.Drawing.Size(52, 17);
            this.Version113cRadioButton.TabIndex = 0;
            this.Version113cRadioButton.TabStop = true;
            this.Version113cRadioButton.Text = "1.13c";
            this.Version113cRadioButton.UseVisualStyleBackColor = true;
            this.Version113cRadioButton.Click += new System.EventHandler(this.VersionRadioButton_Click);
            // 
            // Version113dRadioButton
            // 
            this.Version113dRadioButton.AutoSize = true;
            this.Version113dRadioButton.Location = new System.Drawing.Point(3, 26);
            this.Version113dRadioButton.Name = "Version113dRadioButton";
            this.Version113dRadioButton.Size = new System.Drawing.Size(52, 17);
            this.Version113dRadioButton.TabIndex = 0;
            this.Version113dRadioButton.TabStop = true;
            this.Version113dRadioButton.Text = "1.13d";
            this.Version113dRadioButton.UseVisualStyleBackColor = true;
            this.Version113dRadioButton.Click += new System.EventHandler(this.VersionRadioButton_Click);
            // 
            // StartGameButton
            // 
            this.StartGameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.StartGameButton.Location = new System.Drawing.Point(26, 187);
            this.StartGameButton.Name = "StartGameButton";
            this.StartGameButton.Size = new System.Drawing.Size(188, 37);
            this.StartGameButton.TabIndex = 2;
            this.StartGameButton.Text = "Start Diablo II";
            this.StartGameButton.UseVisualStyleBackColor = true;
            this.StartGameButton.Click += new System.EventHandler(this.StartGameButton_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel7);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel2.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel2.Controls.Add(this.StartGameButton);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(244, 230);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Location = new System.Drawing.Point(61, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(117, 80);
            this.panel1.TabIndex = 4;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.GameVersionLabel);
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel6);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(117, 80);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.FullscreenCheckBox);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 55);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(109, 23);
            this.flowLayoutPanel6.TabIndex = 6;
            // 
            // FullscreenCheckBox
            // 
            this.FullscreenCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.FullscreenCheckBox.AutoSize = true;
            this.FullscreenCheckBox.Location = new System.Drawing.Point(32, 3);
            this.FullscreenCheckBox.Name = "FullscreenCheckBox";
            this.FullscreenCheckBox.Size = new System.Drawing.Size(74, 17);
            this.FullscreenCheckBox.TabIndex = 5;
            this.FullscreenCheckBox.Text = "Fullscreen";
            this.FullscreenCheckBox.UseVisualStyleBackColor = true;
            this.FullscreenCheckBox.CheckedChanged += new System.EventHandler(this.FullscreenCheckBox_CheckedChanged);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel7.AutoSize = true;
            this.flowLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel7.Controls.Add(this.NumberOfWindowsComboBox);
            this.flowLayoutPanel7.Controls.Add(this.WindowLabel);
            this.flowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(9, 89);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(228, 27);
            this.flowLayoutPanel7.TabIndex = 5;
            // 
            // NumberOfWindowsComboBox
            // 
            this.NumberOfWindowsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumberOfWindowsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NumberOfWindowsComboBox.FormattingEnabled = true;
            this.NumberOfWindowsComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.NumberOfWindowsComboBox.Location = new System.Drawing.Point(115, 3);
            this.NumberOfWindowsComboBox.MaxDropDownItems = 4;
            this.NumberOfWindowsComboBox.MaxLength = 2;
            this.NumberOfWindowsComboBox.Name = "NumberOfWindowsComboBox";
            this.NumberOfWindowsComboBox.Size = new System.Drawing.Size(110, 21);
            this.NumberOfWindowsComboBox.TabIndex = 1;
            this.NumberOfWindowsComboBox.SelectedValueChanged += new System.EventHandler(this.NumberOfWindowsComboBox_SelectedValueChanged);
            // 
            // WindowLabel
            // 
            this.WindowLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WindowLabel.AutoSize = true;
            this.WindowLabel.Location = new System.Drawing.Point(3, 7);
            this.WindowLabel.Name = "WindowLabel";
            this.WindowLabel.Size = new System.Drawing.Size(106, 13);
            this.WindowLabel.TabIndex = 0;
            this.WindowLabel.Text = "Number of Windows:";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel4.AutoSize = true;
            this.flowLayoutPanel4.Controls.Add(this.MapHackComboBox);
            this.flowLayoutPanel4.Controls.Add(this.MaphackConfigLabel);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(25, 122);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(212, 27);
            this.flowLayoutPanel4.TabIndex = 3;
            // 
            // MapHackComboBox
            // 
            this.MapHackComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MapHackComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MapHackComboBox.FormattingEnabled = true;
            this.MapHackComboBox.Items.AddRange(new object[] {
            ""});
            this.MapHackComboBox.Location = new System.Drawing.Point(99, 3);
            this.MapHackComboBox.Name = "MapHackComboBox";
            this.MapHackComboBox.Size = new System.Drawing.Size(110, 21);
            this.MapHackComboBox.TabIndex = 1;
            this.MapHackComboBox.SelectedValueChanged += new System.EventHandler(this.MapHackComboBox_SelectedValueChanged);
            // 
            // MaphackConfigLabel
            // 
            this.MaphackConfigLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.MaphackConfigLabel.AutoSize = true;
            this.MaphackConfigLabel.Location = new System.Drawing.Point(3, 7);
            this.MaphackConfigLabel.Name = "MaphackConfigLabel";
            this.MaphackConfigLabel.Size = new System.Drawing.Size(90, 13);
            this.MaphackConfigLabel.TabIndex = 0;
            this.MaphackConfigLabel.Text = "MapHack Config:";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.AutoSize = true;
            this.flowLayoutPanel5.Controls.Add(this.AdditionalParamsLabel);
            this.flowLayoutPanel5.Controls.Add(this.AdditionalParamsTextBox);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 155);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(234, 26);
            this.flowLayoutPanel5.TabIndex = 3;
            // 
            // AdditionalParamsLabel
            // 
            this.AdditionalParamsLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.AdditionalParamsLabel.AutoSize = true;
            this.AdditionalParamsLabel.Location = new System.Drawing.Point(3, 6);
            this.AdditionalParamsLabel.Name = "AdditionalParamsLabel";
            this.AdditionalParamsLabel.Size = new System.Drawing.Size(112, 13);
            this.AdditionalParamsLabel.TabIndex = 0;
            this.AdditionalParamsLabel.Text = "Additional Parameters:";
            // 
            // AdditionalParamsTextBox
            // 
            this.AdditionalParamsTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.AdditionalParamsTextBox.Location = new System.Drawing.Point(121, 3);
            this.AdditionalParamsTextBox.Name = "AdditionalParamsTextBox";
            this.AdditionalParamsTextBox.Size = new System.Drawing.Size(110, 20);
            this.AdditionalParamsTextBox.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 230);
            this.Controls.Add(this.flowLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.Text = "HDD2 Launcher";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem FileMenuItem;
        private System.Windows.Forms.MenuItem HelpMenuItem;
        private System.Windows.Forms.MenuItem AboutMenuItem;
        private System.Windows.Forms.Label GameVersionLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton Version113cRadioButton;
        private System.Windows.Forms.RadioButton Version113dRadioButton;
        private System.Windows.Forms.Button StartGameButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.ComboBox MapHackComboBox;
        private System.Windows.Forms.Label MaphackConfigLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label AdditionalParamsLabel;
        private System.Windows.Forms.TextBox AdditionalParamsTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.CheckBox FullscreenCheckBox;
        private System.Windows.Forms.MenuItem ExitMenuItem;
        private System.Windows.Forms.MenuItem OptionsMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label WindowLabel;
        private System.Windows.Forms.ComboBox NumberOfWindowsComboBox;
    }
}

