﻿namespace HDD2_Launcher.Source {
    #region Using Imports
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using HDD2_Launcher.Properties;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    #endregion Using Imports
    public partial class MainForm : Form {
        private ConfigManager BHConfigManager {
            get;
            set;
        }

        public MainForm() {
            InitializeComponent();

            // Check boxes and proper radio buttons
            this.Version113dRadioButton.Checked = Settings.Default.Version113dSelected;
            this.Version113cRadioButton.Checked = !this.Version113dRadioButton.Checked;
            this.FullscreenCheckBox.Checked = Settings.Default.Fullscreen;
            this.AdditionalParamsTextBox.Text = Settings.Default.AddParams;

            // Detect config and set values.
            this.BHConfigManager = new ConfigManager();
            this.BHConfigManager.FindConfigs();
            foreach (string config in this.BHConfigManager.BHConfigs) {
                this.MapHackComboBox.Items.Add(config);
            }
            this.MapHackComboBox.SelectedIndex = this.MapHackComboBox.Items.IndexOf(Settings.Default.MHConfigFile);

            // Detect Game.exe if first time launch.
            if (string.IsNullOrWhiteSpace(Settings.Default.GameInstallLoc) || !File.Exists(Settings.Default.GameInstallLoc + @"\game.exe")) {
                string gameExeLocation = GameInstallLocator.GetInstallLocation();
                if (string.IsNullOrWhiteSpace(gameExeLocation)) {
                    MessageBox.Show("Game.exe could not be found in found using the registry. Please specify its location manually.", "Game.exe not detected.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    var optionsMenu = new OptionsForm();
                    optionsMenu.ShowDialog(this);
                } else {
                    Settings.Default.GameInstallLoc = gameExeLocation;
                }
            }

            this.NumberOfWindowsComboBox.Text = Math.Max(Math.Min(Settings.Default.NumStartWindows, 4), 1).ToString();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e) {
            string additionalParamCleaned = AdditionalParamsTextBox.Text.ToLower().Trim();
            Settings.Default.AddParams = additionalParamCleaned;

            Settings.Default.Save();
        }

        private void VersionRadioButton_Click(object sender, EventArgs e) {
            Settings.Default.Version113dSelected = this.Version113dRadioButton.Checked;
        }

        private void FullscreenCheckBox_CheckedChanged(object sender, EventArgs e) {
            Settings.Default.Fullscreen = this.FullscreenCheckBox.Checked;
        }

        private void StartGameButton_Click(object sender, EventArgs e) {
            // Check that Game.exe can be found.
            if (string.IsNullOrWhiteSpace(Settings.Default.GameInstallLoc) || !Directory.Exists(Settings.Default.GameInstallLoc) || !File.Exists(Settings.Default.GameInstallLoc + @"\Game.exe")) {
                MessageBox.Show("Game.exe has not been specified. Please specify its location.", "Game.exe not found.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                var optionsMenu = new OptionsForm();
                optionsMenu.ShowDialog(this);
                return;
            }

            // Check that the number of windows is valid.
            int numberOfWindows = 0;
            if (!int.TryParse(this.NumberOfWindowsComboBox.Text, out numberOfWindows) || numberOfWindows == 0) {
                return;
            }

            // Choose the correct hack location based on the version selected.
            string hackPath = (Settings.Default.Version113dSelected) ? @"Files\BHMH_113d" : @"Files\BHMH_113c";

            // Copy the specified config file.
            string configFile = Settings.Default.MHConfigsLoc + Settings.Default.MHConfigFile + ".cfg";

            if (string.IsNullOrWhiteSpace(Settings.Default.MHConfigFile) && File.Exists(hackPath + @"\BH.cfg")) {
                File.Delete(hackPath + @"\BH.cfg");
            } else if (File.Exists(configFile)) {
                File.Copy(Settings.Default.MHConfigsLoc + Settings.Default.MHConfigFile + ".cfg", hackPath + @"\BH.cfg", true);
            }

            // Create process start info.
            Process diablo2Process = new Process();
            diablo2Process.StartInfo.UseShellExecute = false;
            diablo2Process.StartInfo.WorkingDirectory = @"Files";
            diablo2Process.StartInfo.FileName = Settings.Default.GameInstallLoc + @"\Game.exe";

            // Build the game command arguments.
            StringBuilder argumentsBuilder = new StringBuilder(" -txt -direct");
            if (!FullscreenCheckBox.Checked) {
                argumentsBuilder.Append(" -w");
            }

            // Prevent Glide and Direct3D from being specified.
            string additionalParamCleaned = AdditionalParamsTextBox.Text.ToLower().Trim();
            argumentsBuilder.Append(" " + additionalParamCleaned);
            diablo2Process.StartInfo.Arguments = argumentsBuilder.ToString();

            // Start Diablo II.
            for (int i = 0; i < numberOfWindows; i++) {
                diablo2Process.Start();
            }

            Close();

            // Start hack injector.
            if (!Settings.Default.EnableHacks) {
                return;
            }
            Process injectorProcess = new Process();
            injectorProcess.StartInfo.Verb = "runas";
            injectorProcess.StartInfo.UseShellExecute = false;
            injectorProcess.StartInfo.RedirectStandardOutput = true;
            injectorProcess.StartInfo.FileName = hackPath + @"\BH.Injector.exe";
            injectorProcess.StartInfo.WorkingDirectory = Path.GetFullPath(hackPath);
            injectorProcess.StartInfo.Arguments = @"-o 0 -p";

            Thread.Sleep(2000);
            DialogResult dialogResult;
            do {
                dialogResult = MessageBox.Show("Diablo 2 is taking too long to start up. Exit?", "Exit Diablo II?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                Thread.Sleep(2000);
            } while (Process.GetProcessesByName("Game.exe").Length < numberOfWindows && dialogResult != System.Windows.Forms.DialogResult.OK);

            injectorProcess.Start();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e) {
            Close();
        }

        private void OptionsMenuItem_Click(object sender, EventArgs e) {
            var optionsMenu = new OptionsForm();
            optionsMenu.ShowDialog(this);
        }

        private void MapHackComboBox_SelectedValueChanged(object sender, EventArgs e) {
            Settings.Default.MHConfigFile = this.MapHackComboBox.Text;
        }

        private void NumberOfWindowsComboBox_SelectedValueChanged(object sender, EventArgs e) {
            int numberOfWindows = 1;
            if (int.TryParse(this.NumberOfWindowsComboBox.Text, out numberOfWindows)) {
                Settings.Default.NumStartWindows = numberOfWindows;
            }
        }
    }
}
