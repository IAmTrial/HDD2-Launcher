﻿namespace HDD2_Launcher.Source {
    #region Using Imports
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Win32;
    #endregion Using Imports
    public class GameInstallLocator {
        private static List<string> REGISTRY_KEY_LOCATIONS = new List<string>();
        static GameInstallLocator() {
            REGISTRY_KEY_LOCATIONS.Add(@"HKEY_LOCAL_MACHINE\SOFTWARE\Blizzard Entertainment\Diablo II");
            REGISTRY_KEY_LOCATIONS.Add(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Blizzard Entertainment\Diablo II");
            REGISTRY_KEY_LOCATIONS.Add(@"HKEY_CURRENT_USER\Software\Blizzard Entertainment\Diablo II");
            REGISTRY_KEY_LOCATIONS.Add(@"HKEY_CURRENT_USER\Software\Classes\VirtualStore\MACHINE\SOFTWARE\Blizzard Entertainment\Diablo II");
        }

        public static string GetInstallLocation() {
            foreach (string location in REGISTRY_KEY_LOCATIONS) {
                object value = Registry.GetValue(location, "InstallPath", null);
                if (value != null) {
                    return value.ToString();
                }
            }

            return null;
        }
    }
}
