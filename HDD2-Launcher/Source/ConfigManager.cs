﻿namespace HDD2_Launcher.Source {
    #region Using Imports
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using HDD2_Launcher.Properties;
    #endregion Using Imports
    public class ConfigManager {
        public ISet<String> BHConfigs {
            get;
            private set;
        }

        public ConfigManager() {
            this.BHConfigs = new SortedSet<String>();
        }

        public void FindConfigs() {
            if (!Directory.Exists(Settings.Default.MHConfigsLoc)) {
                Directory.CreateDirectory(Settings.Default.MHConfigsLoc);
            }

            foreach (string configFile in Directory.GetFiles(Settings.Default.MHConfigsLoc)) {
                string configFileCorrected = configFile.Trim().ToLower();
                if (configFileCorrected.EndsWith(".cfg")) {
                    string fileName = Path.GetFileName(configFile);
                    fileName = fileName.Substring(0, fileName.Length - 4);
                    this.BHConfigs.Add(fileName);
                }
            }
        }
    }
}
