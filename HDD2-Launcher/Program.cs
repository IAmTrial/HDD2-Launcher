﻿namespace HDD2_Launcher {
    #region Using Imports
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using HDD2_Launcher.Source;
    #endregion Using Imports
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
